# Pronunciation Dictionary

Pronunciation Dictionary for 12 Indian languages. Mapping of words in 12 Indian languages to the corresponding common label set representation. This is created using unified-parser.

# Common Label Set (CLS)
All the sounds or phonemes of 13 Indian languages are listed. Similar sounds across different languages are mapped together and denoted by a single label. These labels are represented using the Roman alphabet set. Since the number of sounds in a language exceeds the number of roman characters, certain suffixes are used. Suffix x is used to denote retroflex place of articulation, for eg: /d/ vs /dx/. For aspiration, suffix h is used, for, eg: /g/ vs /gh/. Certain sounds are language-specific. They are represented by separate labels. /zh/ is predominant in Tamil and Malayalam. Certain phonemes in Hindi like /kq/, /khq/ have been added to account for some pronunciations in foreign languages. Marathi has both dental and palatal affricates (/c/, /cx/, /j/, /jx/) compared to other languages which have only palatal affricates (/c/, /j/). Some languages have both phoneme and grapheme representations; this is to ensure that the native script is largely recoverable from the transliterated text. The International Phonetic Alphabet (IPA) symbols are used as references.

- [CLS mapping](https://www.iitm.ac.in/donlab/tts/cls.php)

# Unified Parser
This parser attempts to unify the languages based on the Common Label Set. It is designed across all the languages capitalising on the syllable structure of Indian languages. The Unified Parser converts UTF-8 text to common label set, applies letter-to-sound rules and generates the corresponding phoneme sequences.The effort is a step towards natural language understanding system that operates on Indian languages and generates the parsed output. This structured method requires only knowledge of the basic language. With good lexicons it is possible to get more than 95% correctness of words in a language. This method can be further extended for a number of other Indian languages in minimal time and effort. Given the unity in the diversity of Indian languages, developing parsers for new languages is easy using the unified approach.

- [Unified parser](https://www.iitm.ac.in/donlab/tts/unified.php)

# Publication
- Ramani, B., S. Lilly Christina, G. Anushiya Rachel, V. Sherlin Solomi, Mahesh Kumar Nandwana, Anusha Prakash, S. Aswin Shanmugam et al. "A common attribute based unified HTS framework for speech synthesis in Indian languages." In Eighth ISCA Workshop on Speech Synthesis. 2013.
- Baby, Arun, Nishanthi NL, Anju Leela Thomas, and Hema A. Murthy. "A unified parser for developing Indian language text to speech synthesizers." In International Conference on Text, Speech, and Dialogue, pp. 514-521. Springer, Cham, 2016.

# Developed and Created by
SMT Lab, IIT Madras
